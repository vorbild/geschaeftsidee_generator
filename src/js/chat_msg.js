var chat_msg = function(settings, translation){

  var my_class = this;

  this.setSendMsg = function(user, content){



    var ret = "";

    switch( user ){

      case "user":
      ret = my_class.getMsgUser(content, my_class.getTime() );

      break;

      case "bot":
       ret = my_class.getMsgBot(content, my_class.getTime() );
      break;

    }

    return ret;
  };

  this.getTime = function(){

    if(typeof settings.getTime == "function")
      return settings.getTime();

    var date = new Date();

    var time = [date.getHours(), date.getMinutes()];

    if(time[0] <= 9)
      time[0] = "0"+time[0];

      if(time[1] <= 9)
        time[1] = "0"+time[1];

      return time[0]+":"+time[1]+" Uhr";

  };

  this.getMergeContent = function(content){


    if( typeof content == "string" )
      return content;

    var txt = "";

    if(typeof content.msg != "undefined")
      txt += content.msg;

    if( typeof content.action != "undefined")
      txt += '<p><span class="chat_action">'+content.action+'</span></p>';

    return txt;

  };

  this.getMsgUser = function(content, date){



    var html = '';

    html += '<li class="you bounceIn">';
    html += '<span class="user"><img src="'+settings.user.img+'" title="'+settings.user.name+'" /></span>';
    html += '<div class="date">';
    html += date;
    html += '</div>';
    html += '<div class="message">';
    html += '<p>';
    html += my_class.getMergeContent(content);
    html += '</p>';
    html += '</div>';
    html += '</li>';

    return html;
  };

  this.getMsgBot = function(content, date){

    var html = '';

    html += '<li class="other bounceIn" >';
    html += '<span class="user"><img src="'+settings.bot.img+'" title="'+settings.bot.name+'" /></span>';
    html += '<div class="date">';
    html += date;
    html += '</div>';
    html += '<div class="message">';

    html += '<p>';
    html += my_class.getMergeContent(content);
    html += '</p>';
    html += '</div>';
    html += '</li>';

    return html;

  };

  this.getSpinner = function(){

    var html = '';

    html += "<span class='spinme-right chat-loading'>";
    html += settings.bot.name+" "+translation.is_writing;
    html += "<div class='spinner'>";
    html += "<div class='bounce1'></div>";
    html += "<div class='bounce2'></div>";
    html += "<div class='bounce3'></div>";
    html += "</div>";
    html += "</span>";

    return html;
  };


};
