"use strict";

var chat = function chat(that, settings) {

  var my_class = this;

  my_class.settings = settings;

  my_class.step = 0;
  my_class.responses = {};

  this.run = function () {

    my_class.getData(my_class.settings.translation, function (data) {
      my_class.translate = data;my_class.getContainer();my_class.msg = new chat_msg(my_class.settings, data);

      my_class.getData(my_class.settings.data, function (data) {

        my_class.chat_msg = data;

        if (typeof my_class.settings.init == "function") my_class.settings.init(my_class);else my_class.start_app();
      });
    });
  };

  this.start_app = function () {

    my_class.try(function () {
      my_class.chat_container.append(my_class.msg.setSendMsg("bot", my_class.chat_msg[my_class.step]));
    });

    my_class.try(function () {
      my_class.getAutoNext(my_class.chat_msg[my_class.step]);
    });
    my_class.try(function () {
      my_class.getAnswerType(my_class.chat_msg[my_class.step]);
    });
    my_class.try(function () {
      my_class.input_container.attr("placeholder", my_class.chat_msg[my_class.step].placeholder);
    });

    my_class.getMsgListener(function (response) {

      my_class.responses[my_class.step] = response;

      my_class.settings.response(my_class);
    });
  };

  this.getAnswerType = function (part) {

    if (typeof part.type == "undefined") return false;

    switch (part.type) {

      case "select":
        $(that).find("#input #msg").replaceWith('<select name="msg" id="msg"></select>');

        $.each(part.select, function (k, v) {
          $(that).find("#input #msg").append("<option value='" + k + "'>" + v + "</option>");
        });
        my_class.input_container = $(that).find("#input select");

        break;

      default:

        $(that).find("#input #msg").replaceWith('<input type="text" name="msg" id="msg"/>');
        my_class.input_container = $(that).find("#input input");

    }
  };

  this.try = function (tryme) {

    try {
      tryme();
    } catch (error) {

      //console.log(error);
    }
  };

  this.getAutoNext = function (data) {

    if (typeof data.next == "undefined") return false;

    my_class.step = data.next;
    my_class.loading();

    setTimeout(function () {

      my_class.start_app();
      my_class.scrollDwn();
      my_class.delLoading();
    }, data.timer);
  };

  this.start_app2 = function () {

    my_class.getMsgListener(function (response) {
      my_class.user_input(response);
    });
  };

  this.user_input = function (resp) {

    my_class.responses[my_class.step] = resp;

    //my_class.step++;
    my_class.scrollDwn();
  };

  this.scrollDwn = function () {

    my_class.chat_container.closest("#chat_stage").animate({
      scrollTop: my_class.chat_container.closest("#chat_stage")[0].scrollHeight
    }, 'slow');
  };

  this.loading = function () {
    my_class.input_container.prop("disabled", "true");
    my_class.chat_container.closest("#chat_stage").append(my_class.msg.getSpinner());
  };

  this.delLoading = function () {

    my_class.chat_container.closest("#chat_stage").find(".chat-loading").remove();
    my_class.input_container.removeAttr("disabled");
    my_class.input_container.focus();
  };

  this.getMsgListener = function (callback) {

    my_class.input_container.closest("#input").find("form.input").submit(function () {

      var msg = $(this).find("#msg").val();

      if (typeof msg == "undefined" || msg == null || typeof msg.length == "undefined" || msg.length == null || msg.length == 0) return false;

      my_class.chat_container.find(".chat_action").remove();
      my_class.chat_container.append(my_class.msg.setSendMsg("user", msg));

      my_class.scrollDwn();

      $(this).find("#msg").val("");

      if (typeof callback == "function") callback(msg);

      return false;
    });
  };

  this.getData = function (url, callback) {

    $.ajax({
      dataType: "json",
      url: url,
      success: function success(data) {

        if (typeof callback == "function") callback(data);
      }
    });
  };

  this.getContainer = function () {

    var html = '';

    html += '<style>';

    try {
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.other .message { background-color: ' + settings.bot.bg_color + ';}';
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.other .message:after { border-top-color: ' + settings.bot.bg_color + ';}';
    } catch (error) {}

    try {
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.other .message { color: ' + settings.bot.color + ';}';
    } catch (error) {}

    try {
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.you .message { background-color: ' + settings.user.bg_color + ';}';
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.you .message:after { border-top-color: ' + settings.user.bg_color + ';}';
    } catch (error) {}

    try {
      html += '#' + $(that).attr("id") + ' #jj_chat .chat ul li.you .message { color: ' + settings.user.color + ';}';
    } catch (error) {}

    html += '</style>';

    html += '<div id="jj_chat">';
    html += '<div id="chat_stage">';
    html += '<div class="chat">';
    html += '<ul>';
    html += '</ul>';
    html += '</div>';
    html += '</div>';
    html += '<div id="input">';
    html += '<form class="input">';
    html += '<label for="msg">' + my_class.translate.message + '</label>';
    html += '<input type="text" name="msg" id="msg"/>';
    html += '<button type="submit">' + my_class.translate.submit + '</button>';
    html += '</form>';
    html += '</div>';
    html += '</div>';

    $(that).html("");
    $(that).append(html);

    my_class.chat_container = $(that).find("#chat_stage .chat ul");
    my_class.input_container = $(that).find("#input input");
  };
};

$.fn.chat = function (settings) {

  var run_app = new chat(this, settings);
  run_app.run();
};
"use strict";

var chat_msg = function chat_msg(settings, translation) {

  var my_class = this;

  this.setSendMsg = function (user, content) {

    var ret = "";

    switch (user) {

      case "user":
        ret = my_class.getMsgUser(content, my_class.getTime());

        break;

      case "bot":
        ret = my_class.getMsgBot(content, my_class.getTime());
        break;

    }

    return ret;
  };

  this.getTime = function () {

    if (typeof settings.getTime == "function") return settings.getTime();

    var date = new Date();

    var time = [date.getHours(), date.getMinutes()];

    if (time[0] <= 9) time[0] = "0" + time[0];

    if (time[1] <= 9) time[1] = "0" + time[1];

    return time[0] + ":" + time[1] + " Uhr";
  };

  this.getMergeContent = function (content) {

    if (typeof content == "string") return content;

    var txt = "";

    if (typeof content.msg != "undefined") txt += content.msg;

    if (typeof content.action != "undefined") txt += '<p><span class="chat_action">' + content.action + '</span></p>';

    return txt;
  };

  this.getMsgUser = function (content, date) {

    var html = '';

    html += '<li class="you bounceIn">';
    html += '<span class="user"><img src="' + settings.user.img + '" title="' + settings.user.name + '" /></span>';
    html += '<div class="date">';
    html += date;
    html += '</div>';
    html += '<div class="message">';
    html += '<p>';
    html += my_class.getMergeContent(content);
    html += '</p>';
    html += '</div>';
    html += '</li>';

    return html;
  };

  this.getMsgBot = function (content, date) {

    var html = '';

    html += '<li class="other bounceIn" >';
    html += '<span class="user"><img src="' + settings.bot.img + '" title="' + settings.bot.name + '" /></span>';
    html += '<div class="date">';
    html += date;
    html += '</div>';
    html += '<div class="message">';

    html += '<p>';
    html += my_class.getMergeContent(content);
    html += '</p>';
    html += '</div>';
    html += '</li>';

    return html;
  };

  this.getSpinner = function () {

    var html = '';

    html += "<span class='spinme-right chat-loading'>";
    html += settings.bot.name + " " + translation.is_writing;
    html += "<div class='spinner'>";
    html += "<div class='bounce1'></div>";
    html += "<div class='bounce2'></div>";
    html += "<div class='bounce3'></div>";
    html += "</div>";
    html += "</span>";

    return html;
  };
};