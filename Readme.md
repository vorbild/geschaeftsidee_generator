# Anleitung
Der Geschäftsidee-Generator ist ein Tool zur Inspiration für neue Geschäftsideen. Bevor der Ideen-Generator loslegt, kann der User via Chat verschiedene Parameter bestimmen, um so die automatische Ideenauswahl vorzufiltern. Er basiert auf Grundlagen der Archetypen-Forschung, allgemeinem Marketingwissen und einem Zufallsgenerator.

## Nutzen
Mittels Zufallsgenerator werden verschiedene Wörter miteinander kombiniert. Diese können Teils unbrauchbar sein, Teils aber auch sehr inspirativ. Daher lohnt es sich den Generator auszutesten und sich durch verschiedene Geschäftsideen zu klicken. Vielleicht entsteht daraus ja eine spannende Idee, die die Welt der Zielgruppe wirklich bereichert...

## Installation
```
git clone https://bitbucket.org/vorbild/geschaeftsidee_generator.git
cd geschaeftsidee_generator;
```

### Start-Datei
app/index.html

### Demo-Datei
app/demo/iframe.html

## Bearbeitung im src-Verzeichnis
```
npm install;
gulp watch;
```

## Version
Siehe package.json
