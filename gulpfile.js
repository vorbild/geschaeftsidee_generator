
var fs = require('fs');

var project = JSON.parse(fs.readFileSync('./package.json'));

var	gulp = require('gulp'),
	watch = require('gulp-watch'),
	scss = require('gulp-scss'),
	cleanCSS = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	rename = require('gulp-rename'),
	babel = require('gulp-babel'),
	concat = require('gulp-concat'),
	minify = require('gulp-minify'),
//	iconfont = require('gulp-iconfont'),
 //	iconfontCSS = require('gulp-iconfont-css'),
//	fontName = project.name+'-font',
//	imageResize = require('gulp-image-resize'),
//  imagemin = require ('gulp-imagemin'),
	browserSync = require('browser-sync'),
//	connect = require("gulp-connect-php"),
	removeFiles = require("gulp-remove-files");

	var imgSrc = './src/images/full/**/*',
	    imgDst = './app/images';

gulp.task('scss', function(){

	gulp.src('./app/css/*')
    .pipe(removeFiles());

	return gulp.src("./src/sass/style.scss")
	.pipe(scss())
	.pipe(autoprefixer())
	.pipe(rename({ basename: 'style'}))
	.pipe(gulp.dest("./app/css"))
	.pipe(cleanCSS())
	.pipe(rename({ basename: 'style-min'}))
	.pipe(gulp.dest("./app/css"))
	.pipe(browserSync.stream());
});

gulp.task('js', function(){

	gulp.src('./src/js/*.js')
	.pipe(babel({presets: ['es2015']}))
	.pipe(concat('global.js'))
	.pipe(gulp.dest("./app/js"))
	.pipe(minify())
	.pipe(gulp.dest("./app/js"))
	.pipe(browserSync.stream());

	return gulp.src('./app/js/global.js')
    .pipe(removeFiles());

});

gulp.task('php', function(){
		return  gulp.src('./src/classes/**/*.php')
	//.pipe(babel({presets: ['es2015']}))
	.pipe(minify())
	.pipe(gulp.dest("./app/classes"))
	.pipe(browserSync.stream());


});

gulp.task('json', function(){
		return  gulp.src('./src/json/*.json')
	.pipe(gulp.dest("./app/json"))
	.pipe(browserSync.stream());
});

gulp.task('html', function(){
		return  gulp.src('./src/**/*.html')
	//.pipe(babel({presets: ['es2015']}))
	.pipe(minify())
	.pipe(gulp.dest("./app"))
	.pipe(browserSync.stream());


});

 gulp.task('iconfont', function() {
	gulp.src('./src/images/font/svg/*.svg')
		 .pipe(iconfontCSS({
			 fontName: fontName,
			 cssClass: 'test',
			 targetPath: '../../src/sass/global/_'+project.name+'-iconfont.scss',
			 fontPath: './app/fonts/'
		 }))
		 .pipe(iconfont({
			 fontName: fontName,
			 // Remove woff2 if you get an ext error on compile
			 formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
			 normalize: true,
			 fontHeight: 1001
		 }))
		 .pipe(gulp.src('./app/fonts/'))
		 .pipe(browserSync.stream());

		  return gulp.parallel('scss');
 });



 gulp.task('img-thumb', function () {
    gulp.src(imgSrc)
        .pipe(imageResize({
            width : 500,
            height : 281,
            crop : true,
            upscale : false,
            imageMagick: true
        }))
        //.pipe(imagemin())
        .pipe(rename({suffix: '-thumb'}))
        .pipe(gulp.src(imgDst));
});

gulp.task('img-large', function () {
    gulp.src(imgSrc)
        .pipe(imageResize({
            width : 1000,
            height : 700,
            crop : true,
            upscale : false,
            imageMagick: true
        }))
        //.pipe(imagemin())
        .pipe(rename({suffix: '-large'}))
        .pipe(gulp.src(imgDst));
});

gulp.task('watch', function() {


	browserSync.init({
			server: "./app",
			baseDir: './app',
			open: false,
    //host: 'mysite.com',
    //host: 'test.com:80',




	});

	  gulp.src('./node_modules/jquery/dist/jquery.min.js').pipe(gulp.dest("./app/js"));

    gulp.watch('./src/classes/*.php', gulp.parallel('php'));
    gulp.watch('./src/json/*.json', gulp.parallel('json'));
    gulp.watch('./src/**/*.html', gulp.parallel('html'));
    gulp.watch('./src/js/*.js', gulp.parallel('js'));
    gulp.watch('./src/sass/**/*.scss', gulp.parallel('scss'));
    gulp.watch('./src/images/font/svg/*.svg', gulp.parallel('iconfont'));
    gulp.watch('./src/images/full/*.*', gulp.parallel('img-large'));
    gulp.watch('./src/images/full/*.*', gulp.parallel('img-thumb'));

 });
